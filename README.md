# Introduction
This project uses a Nifi DSL in order to build a nifi dataflow. That data flow is composed of two processors "GetFile" and "PutFile" and data will 
be transfered from a source path inserted as a property in GetFile processor to a destination path inserted as a property in PutFile processor. 

# Example used as a DSL Nifi
```clojure
{:flow [:in :out [:success]]
          :processors [{:id   :in
                        :name :GetFile
                        :type :org.apache.nifi.processors.standard.GetFile
                        :from "/tmp/from"}
                       {:id            :out
                        :name :PutFile
                        :type :org.apache.nifi.processors.standard.PutFile
                        :to    "/tmp/to"
                        :autoterminate :failure}]
          :relationships [{:id :success
                           :backpressure {:obj-threshold 1000 :size-threshold "1 MB"}}]}
```
# Project                           
this project consists of two namespaces:
- Nifi-handler: which contains abstract methods to deal with Nifi (create-processor, set-properties-processor, start-processor, ...)
- Nifi-mv : which represents the mv namespace that will use abstract methods of nifi-handler

# Nifi-handler

As introduced before, Nifi handler is a namespace to use to achieve a specific purpose like in our case "mv" feature. It consists of various features
that will be represented in this table:


| Feature      |function in project | explication                       |
|:--------------|:---|:----------------------------------|
| create a processor      | `(create-processor clientId type name url-processors-creation)`| This function creates a processor in the UI, type and name of the processor should be specified. For instance, nifi processor GetFile has as type "org.apache.nifi.processors.standard.GetFile" and name "GetFile"
| start a processor |`(start-processor client-id component-name url-processor url-processors)`  |starts a processor. the clientId is the id of the user who wants to make this change. Only the name of the processor should be specified
| autoterminates a processor on a specific state| ` (auto-terminate-processor-on-state client-id component-name state url-processor url-processors)`|  if in the DSL Nifi, autoterminate field is present, the processor is autoterminated of the state given
| connect two processors on success      | `(connect-two-processors-on-success client-id processor-source-name processor-dest-name requests-url relationship data-size object-threshold url-connections url-processors)`|This function connects two processors on the given relationship in the DSL. Source and destination of the connection should be specified. 
| set-properties to a processor | `(set-properties-processor name-processor client-id properties url-processor url-processors)` | this function sets the properties of a processor given by its name. Properties should contain the properties of the processor in Nifi
| create a process group |`create-process-group client-id name-process-group url-process-groups`| this function creates a process group in Nifi by name.

# Nifi-mv
This is the core of our project where the main function will be executed. It consists of various specific functions that will be explained in the next table:

| Feature      |function | Explication                       |
|:--------------|:---|:----------------------------------|
| get the name of processor by id |`(get-name-processor-by-id processors id)` | based on the id of processor in the DSL Nifi, the name of the processor will be returned.
| create the processors needed | `(create-spec-processors processors client-id url-processors-creation)` | this function will retrieve the processors section in the DSL Nifi and will create the needed processors.
| set the properties for the processors (GetFile)|`(set-properties-processors processors client-id url-processor url-processors)`| In order to do the "mv" feature, we have to set the "Input Directory" property of GetFile and "Directory" property of PutFile
| auto-terminates-processors |`auto-terminate-processors processors client-id url-processor url-processors`| If a processor in the processors section has an autoterminate field, it will be autoterminated on the state given. In our case, it is only the PutFile processor
| connect processors |`connect-processors flow processors relationships client-id requests-url url-connections url-processors` | based on the ids of processors defined in flow section, this processors will be connected on the relationship that is also given in flow section
| start processors |`start-processors-flow flow processors client-id url-processor url-processors`| this function will start all the valid processor of the flow
| manage urls |`(manage-urls url-nifi name-process-group url-process-groups)`| generate specific nifi-api urls dependending on the name of the process group 
| move feature| `mv-nifi spec client-id url-processor url-processors url-processors-creation requests-url url-connections`| this represents the main features will call all the features mentioned before

## Contact
ahmed.werghi@oscaro.com or ahmed.werghi@ensi-uma.tn


